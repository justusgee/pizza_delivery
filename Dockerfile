FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install -y python
RUN apt-get install -y python-pip
RUN apt-get clean all

RUN pip install flask

ADD pizza_delivery.py /tmp/pizza_delivery.py
ADD pizza_tests.py /tmp/pizza_tests.py

EXPOSE 5000

CMD ["python","/tmp/pizza_delivery.py"]