#!/usr/bin/env python
from pizza_delivery import app, handle
import unittest
import json


class PizzaTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.headers = [('Content-Type', 'application/json')]

    def test_order_pizza(self):
        # test with error topping
        response = self.app.post('/pizza/api/v1.0/orders', headers=self.headers, data=json.dumps(dict(topping='TEST')))
        assert "400 BAD REQUEST" == response.status
        response_data = json.loads(response.data)
        assert "Calling API is invalid" == response_data['error']

        # test with valid creating order
        response = self.app.post('/pizza/api/v1.0/orders', headers=self.headers, data=json.dumps(dict(topping='SUPREME')))
        assert "201 CREATED" == response.status
        response_data = json.loads(response.data)
        assert "SUPREME" == response_data['topping']

    def test_query_order(self):
        # test with invalid order id
        response = self.app.get('/pizza/api/v1.0/orders/10')
        assert "404 NOT FOUND" == response.status

        response = self.app.post('/pizza/api/v1.0/orders', headers=self.headers, data=json.dumps(dict(topping='CHEESE')))
        response_data = json.loads(response.data)
        order_id = response_data['id']

        # test with retrieving order
        response = self.app.get('/pizza/api/v1.0/orders/{}'.format(order_id))
        assert "200 OK" == response.status
        response_data = json.loads(response.data)
        assert "CHEESE" == response_data['topping']

    def test_pickup_pizza(self):
        response = self.app.get('/pizza/api/v1.0/orders')
        response_data = json.loads(response.data)
        prev_num = len(response_data['orders'])

        response = self.app.post('/pizza/api/v1.0/orders', headers=self.headers, data=json.dumps(dict(topping='MEATLOVERS')))
        response_data = json.loads(response.data)
        order_id = response_data['id']

        # remove the just created order
        response = self.app.delete('/pizza/api/v1.0/orders/{}'.format(order_id))
        assert "200 OK" == response.status

        response = self.app.get('/pizza/api/v1.0/orders')
        response_data = json.loads(response.data)
        assert len(response_data['orders'])==prev_num

    def test_handle(self):
        response = self.app.post('/pizza/api/v1.0/orders', headers=self.headers, data=json.dumps(dict(topping='CHEESE')))
        response_data = json.loads(response.data)
        order_id = response_data['id']

        assert "Invalid json event" == handle(dict(message_type="PIZZA_COOKING"))
        assert "Invalid pizza id" == handle(dict(pizza_id=100, message_type="PIZZA_COOKING"))
        # cannot update status from ORDERED to READY directly
        assert "Invalid pizza status to be updated" == handle(dict(pizza_id=order_id, message_type="PIZZA_READY"))
        assert "Success" == handle(dict(pizza_id=order_id, message_type="PIZZA_COOKING"))
        assert "Success" == handle(dict(pizza_id=order_id, message_type="PIZZA_READY"))

        response = self.app.get('/pizza/api/v1.0/orders/{}'.format(order_id))
        response_data = json.loads(response.data)
        assert "READY" == response_data['status']
        

if __name__ == '__main__':
    unittest.main()
