1. pip install flask
2. chmod +x pizza_delivery.py pizza_tests.py
3. ./pizza_delivery.py  to start restful api, then we can use the following curl command:
    create new order:
    curl -i -H "Content-Type: application/json" -X POST -d '{"topping":"CHEESE"}' http://localhost:5000/pizza/api/v1.0/orders

    retrieve all orders:
    curl -i http://localhost:5000/pizza/api/v1.0/orders

    retrieve specific order:
    curl -i http://localhost:5000/pizza/api/v1.0/orders/1

    if order does not exist, it will return 404 error with json message {"error": "Order Not found"}

    pickup pizza:
    curl -i -H "Content-Type: applcation/json" -X DELETE http://localhost:5000/pizza/api/v1.0/orders/1

    oven integration handle implement:
    from pizza_delivery import handle
    handle(dict(pizza_id=1, message_type="PIZZA_COOKING")) # update pizza with id 1 from status ORDERED to COOKING, return "Success"
    if the pizza id does not exist, it will return "Invalid pizza id"
    if the message_type not in the valid status, for example curret status is ORDERED, message_type is "PIZZA_READY", then it will return "Invalid pizza status to be updated"

4. for unit test, just run ./pizza_tests.py, it will test all the restful api services and handle function

5. for docker image, just run:
  sudo docker pull justusgee/pizza_delivery:1.0
  sudo docker run -p 5000:5000 --net=host -d  justusgee/pizza_delivery:1.0  
  Then we can access the rest api.

  For running unit test in docker image, just run:
  sudo docker run -i -t justusgee/pizza_delivery:1.0 /bin/bash
  Then inside the container:
  cd /tmp
  ./pizza_tests.py
