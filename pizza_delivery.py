#!/usr/bin/env python
from flask import abort, Flask, jsonify, make_response, request

app = Flask(__name__)

STATUS_LIST = ["ORDERED", "COOKING", "READY"]
TOPPING_LIST = ["CHEESE", "SUPREME", "MEATLOVERS"]

orders = []


@app.route('/pizza/api/v1.0/orders', methods=['GET'])
def get_orders():
    """
    Get orders of all the pizzas
    """
    return jsonify({'orders': orders})


@app.route('/pizza/api/v1.0/orders/<int:order_id>', methods=['GET'])
def get_order(order_id):
    """
    Get the order detail of specific order
    """
    order = filter(lambda o: o['id'] == order_id, orders)
    if len(order) == 0:
        abort(404)
    return jsonify(order[0])


@app.errorhandler(404)
def not_found(error):
    """
    Define 404 not found error
    """
    return make_response(jsonify({'error': 'Order Not found'}), 404)


@app.errorhandler(400)
def not_valid(error):
    """
    Define 400 bad request error
    """
    return make_response(jsonify({'error': 'Calling API is invalid'}), 400)


@app.route('/pizza/api/v1.0/orders', methods=['POST'])
def create_order():
    """
    Create new order
    """
    if not request.json or 'topping' not in request.json:
        abort(400)
    if request.json['topping'] not in TOPPING_LIST:
        abort(400)
    order = {
        'id': orders[-1]['id'] + 1 if len(orders) else 1,
        'topping': request.json['topping'],
        'status': STATUS_LIST[0]
    }
    orders.append(order)
    return jsonify(order), 201


@app.route('/pizza/api/v1.0/orders/<int:order_id>', methods=['PUT'])
def update_order(order_id):
    """
    Update existing order
    """
    order = filter(lambda o: o['id'] == order_id, orders)
    if len(order) == 0:
        abort(404)
    if not request.json:
        abort(400)
    if 'topping' in request.json and request.json['topping'] not in TOPPING_LIST:
        abort(400)
    if 'status' in request.json and request.json['status'] not in STATUS_LIST:
        abort(400)

    order[0]['topping'] = request.json.get('topping', order[0]['topping'])
    order[0]['status'] = request.json.get('status', order[0]['status'])
    return jsonify(order[0])


@app.route('/pizza/api/v1.0/orders/<int:order_id>', methods=['DELETE'])
def delete_order(order_id):
    """
    Delete exsting order
    """
    order = filter(lambda o: o['id'] == order_id, orders)
    if len(order) == 0:
        abort(404)
    orders.remove(order[0])
    return jsonify({'result': True})


def handle(json_event):
    """
    Internal function to update pizza status
    """
    if 'pizza_id' not in json_event or 'message_type' not in json_event:
        return "Invalid json event"
    order = filter(lambda o: o['id'] == json_event['pizza_id'], orders)
    if len(order) == 0:
        return "Invalid pizza id"
    if STATUS_LIST.index(order[0]['status']) == (len(STATUS_LIST) - 1) or \
            not json_event['message_type'].endswith(STATUS_LIST[STATUS_LIST.index( \
            order[0]['status']) + 1]):
        return "Invalid pizza status to be updated"
    order[0]['status'] = STATUS_LIST[STATUS_LIST.index(order[0]['status']) + 1]
    return "Success"


if __name__ == '__main__':
    app.run(debug=True)
